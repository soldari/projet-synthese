# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UI/simulatedLedStrip.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPainter, QColor
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView


class SimulatedLedStripOutput(QWidget):
    def __init__(self, parent, output):
        self.parent = parent
        self.device = output
        self.setupUi()
        self.retranslateUi()

    def setupUi(self):
        self.layoutwidget = QtWidgets.QWidget(self)
        self.layoutwidget.setGeometry(QtCore.QRect(0, 0, 500, 60))
        self.layout = QtWidgets.QHBoxLayout(self.layoutwidget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel(self.layoutwidget)
        self.label.setMaximumSize(QtCore.QSize(100, 20))
        self.layout.addWidget(self.label)
        self.frame = QtWidgets.QFrame(self.layoutwidget)
        self.frame.setMaximumSize(QtCore.QSize(2000, 20))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.layout.addWidget(self.frame)
        self.parent.addWidget(self)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Form", self.output.name))

class SimulatedLedStripView(QGraphicsView):
    def __init__(self, parent, output):
        super(SimulatedLedStripView, self).__init__()
        self.painter = QPainter(self)
        self.scene = QGraphicsScene(self)
        self.parent = parent
        self.output = output
        self.setUp()
        self.retranslateUi()
        self.assignActions()
    def setUp(self):
        for i in self.output.ledCount:
            self.scene.addItem(SimulatedLed(self.painter, i))
        self.setScene(self.scene)
    def show(self):
        for (simLed, led) in zip(self.scene.items(), input.leds):
            r, g, b = led["rgb"]
            a = int(led["brightness"]*255)
            color = QColor(r,g,b,a)
            simLed.setColor(color)
        self.show()
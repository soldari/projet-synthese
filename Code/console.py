from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QWidget, QGraphicsView, QGraphicsScene, QGraphicsRectItem
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from model import Model
from constant import *
from installation import *
from simulatedDevices import *
from fxManager import LED_FX_DICTIONARY


class ConsoleWindow(QMainWindow):
    def __init__(self, model: Model):
        super(ConsoleWindow, self).__init__()
        self.model = model
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setupUi(self)
        self.addViewModels()
        self.assignActions()
        self.setUpLogic()

    def setUpLogic(self):
        self.startTimer(TIMER_INTERVAL)
        self.selectedInstallation = self.model.installations['Default installation']
        self.tab_MAIN.setCurrentWidget(self.tab_instal)
        self.updateModel(self.ListModel_Installation,
                         self.model.installationNames())
        self.updateModel(self.ListModel_Device,
                         self.selectedInstallation.deviceNames())
        self.installationsExist()

    def timerEvent(self, event):
        if self.selectedInstallation:
            self.selectedInstallation.fxManager.runAll()
            for simulatedOutput in self.simulatedOutputs:
                simulatedOutput.updateView()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(590, 671)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tab_MAIN = QtWidgets.QTabWidget(self.centralwidget)
        self.tab_MAIN.setEnabled(True)
        self.tab_MAIN.setGeometry(QtCore.QRect(0, 10, 591, 651))
        self.tab_MAIN.setObjectName("tab_MAIN")
        self.tab_instal = QtWidgets.QWidget()
        self.tab_instal.setObjectName("tab_instal")
        self.groupBox_4 = QtWidgets.QGroupBox(self.tab_instal)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 10, 281, 551))
        font = QtGui.QFont()
        font.setFamily("Futura")
        font.setBold(False)
        font.setWeight(50)
        self.groupBox_4.setFont(font)
        self.groupBox_4.setTitle("Installations")
        self.groupBox_4.setObjectName("groupBox_4")
        self.verticalLayoutWidget_3 = QtWidgets.QWidget(self.groupBox_4)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(0, 20, 271, 161))
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.verticalLayout_19 = QtWidgets.QVBoxLayout(
            self.verticalLayoutWidget_3)
        self.verticalLayout_19.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_19.setObjectName("verticalLayout_19")
        self.ListView_installationList = QtWidgets.QListView(
            self.verticalLayoutWidget_3)
        self.ListView_installationList.setObjectName(
            "ListView_installationList")
        self.verticalLayout_19.addWidget(self.ListView_installationList)
        self.pushButton_selectInstallation = QtWidgets.QPushButton(
            self.groupBox_4)
        self.pushButton_selectInstallation.setEnabled(False)
        self.pushButton_selectInstallation.setGeometry(
            QtCore.QRect(0, 400, 271, 51))
        self.pushButton_selectInstallation.setObjectName(
            "pushButton_selectInstallation")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.groupBox_4)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(6, 220, 261, 23))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_27 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_27.setObjectName("label_27")
        self.horizontalLayout_2.addWidget(self.label_27)
        self.lineEdit_installationName = QtWidgets.QLineEdit(
            self.horizontalLayoutWidget)
        self.lineEdit_installationName.setObjectName(
            "lineEdit_installationName")
        self.horizontalLayout_2.addWidget(self.lineEdit_installationName)
        self.label_28 = QtWidgets.QLabel(self.groupBox_4)
        self.label_28.setGeometry(QtCore.QRect(6, 191, 104, 16))
        self.label_28.setObjectName("label_28")
        self.pushButton_addInstallation = QtWidgets.QPushButton(
            self.groupBox_4)
        self.pushButton_addInstallation.setGeometry(
            QtCore.QRect(0, 260, 271, 51))
        self.pushButton_addInstallation.setObjectName(
            "pushButton_addInstallation")
        self.pushButton_deleteInstallation = QtWidgets.QPushButton(
            self.groupBox_4)
        self.pushButton_deleteInstallation.setEnabled(False)
        self.pushButton_deleteInstallation.setGeometry(
            QtCore.QRect(0, 340, 271, 51))
        self.pushButton_deleteInstallation.setObjectName(
            "pushButton_deleteInstallation")
        self.line = QtWidgets.QFrame(self.groupBox_4)
        self.line.setGeometry(QtCore.QRect(6, 320, 259, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.groupBox_Devices = QtWidgets.QGroupBox(self.tab_instal)
        self.groupBox_Devices.setEnabled(False)
        self.groupBox_Devices.setGeometry(QtCore.QRect(300, 10, 281, 551))
        font = QtGui.QFont()
        font.setFamily("Futura")
        font.setBold(False)
        font.setWeight(50)
        self.groupBox_Devices.setFont(font)
        self.groupBox_Devices.setTitle("Devices")
        self.groupBox_Devices.setObjectName("groupBox_Devices")
        self.ListView_deviceListDevices = QtWidgets.QListView(
            self.groupBox_Devices)
        self.ListView_deviceListDevices.setGeometry(
            QtCore.QRect(0, 20, 271, 181))
        self.ListView_deviceListDevices.setObjectName(
            "ListView_deviceListDevices")
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(
            self.groupBox_Devices)
        self.horizontalLayoutWidget_2.setGeometry(
            QtCore.QRect(10, 240, 251, 23))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget_2)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_30 = QtWidgets.QLabel(self.horizontalLayoutWidget_2)
        self.label_30.setObjectName("label_30")
        self.horizontalLayout_3.addWidget(self.label_30)
        self.lineEdit_DeviceName = QtWidgets.QLineEdit(
            self.horizontalLayoutWidget_2)
        self.lineEdit_DeviceName.setObjectName("lineEdit_DeviceName")
        self.horizontalLayout_3.addWidget(self.lineEdit_DeviceName)
        self.checkBox_Pin = QtWidgets.QCheckBox(self.groupBox_Devices)
        self.checkBox_Pin.setGeometry(QtCore.QRect(10, 280, 151, 20))
        self.checkBox_Pin.setObjectName("checkBox_Pin")
        self.lineEdit_Pin = QtWidgets.QLineEdit(self.groupBox_Devices)
        self.lineEdit_Pin.setGeometry(QtCore.QRect(160, 280, 101, 21))
        self.lineEdit_Pin.setObjectName("lineEdit_Pin")
        self.label_18 = QtWidgets.QLabel(self.groupBox_Devices)
        self.label_18.setGeometry(QtCore.QRect(6, 210, 78, 16))
        self.label_18.setObjectName("label_18")
        self.line_2 = QtWidgets.QFrame(self.groupBox_Devices)
        self.line_2.setGeometry(QtCore.QRect(10, 380, 251, 16))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.pushButton_newDeviceOutput = QtWidgets.QPushButton(
            self.groupBox_Devices)
        self.pushButton_newDeviceOutput.setGeometry(
            QtCore.QRect(140, 320, 131, 51))
        self.pushButton_newDeviceOutput.setObjectName(
            "pushButton_newDeviceOutput")
        self.pushButton_DeviceDelete = QtWidgets.QPushButton(
            self.groupBox_Devices)
        self.pushButton_DeviceDelete.setGeometry(
            QtCore.QRect(10, 400, 261, 51))
        self.pushButton_DeviceDelete.setObjectName("pushButton_DeviceDelete")
        self.pushButton_newDeviceInput = QtWidgets.QPushButton(
            self.groupBox_Devices)
        self.pushButton_newDeviceInput.setGeometry(
            QtCore.QRect(10, 320, 111, 51))
        self.pushButton_newDeviceInput.setObjectName(
            "pushButton_newDeviceInput")
        self.tab_MAIN.addTab(self.tab_instal, "")
        self.tab_FX = QtWidgets.QWidget()
        self.tab_FX.setEnabled(False)
        self.tab_FX.setObjectName("tab_FX")
        self.listView_FX = QtWidgets.QListView(self.tab_FX)
        self.listView_FX.setGeometry(QtCore.QRect(10, 30, 331, 161))
        self.listView_FX.setObjectName("listView_FX")
        self.label = QtWidgets.QLabel(self.tab_FX)
        self.label.setGeometry(QtCore.QRect(0, 10, 60, 16))
        self.label.setObjectName("label")
        self.line_4 = QtWidgets.QFrame(self.tab_FX)
        self.line_4.setGeometry(QtCore.QRect(10, 190, 571, 20))
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.label_2 = QtWidgets.QLabel(self.tab_FX)
        self.label_2.setGeometry(QtCore.QRect(0, 200, 81, 16))
        self.label_2.setObjectName("label_2")
        self.comboBox_FXType = QtWidgets.QComboBox(self.tab_FX)
        self.comboBox_FXType.setGeometry(QtCore.QRect(100, 250, 241, 41))
        self.comboBox_FXType.setObjectName("comboBox_FXType")
        self.label_3 = QtWidgets.QLabel(self.tab_FX)
        self.label_3.setGeometry(QtCore.QRect(10, 260, 81, 16))
        self.label_3.setObjectName("label_3")
        self.pushButton_configureParameters = QtWidgets.QPushButton(
            self.tab_FX)
        # self.pushButton_configureParameters.setEnabled(False)
        self.pushButton_configureParameters.setGeometry(
            QtCore.QRect(350, 250, 231, 41))
        self.pushButton_configureParameters.setObjectName(
            "pushButton_configureParameters")
        self.label_4 = QtWidgets.QLabel(self.tab_FX)
        self.label_4.setGeometry(QtCore.QRect(10, 290, 121, 16))
        self.label_4.setObjectName("label_4")
        self.listView_FXAssociatedInputs = QtWidgets.QListView(self.tab_FX)
        self.listView_FXAssociatedInputs.setGeometry(
            QtCore.QRect(20, 310, 321, 101))
        self.listView_FXAssociatedInputs.setObjectName(
            "listView_FXAssociatedInputs")
        self.comboBox_FXInputs = QtWidgets.QComboBox(self.tab_FX)
        self.comboBox_FXInputs.setGeometry(QtCore.QRect(350, 310, 231, 31))
        self.comboBox_FXInputs.setObjectName("comboBox_FXInputs")
        self.pushButton_addAssociatedInput = QtWidgets.QPushButton(self.tab_FX)
        self.pushButton_addAssociatedInput.setGeometry(
            QtCore.QRect(350, 350, 231, 41))
        self.pushButton_addAssociatedInput.setObjectName(
            "pushButton_addAssociatedInput")
        self.label_5 = QtWidgets.QLabel(self.tab_FX)
        self.label_5.setGeometry(QtCore.QRect(10, 420, 121, 16))
        self.label_5.setObjectName("label_5")
        self.listView_FXAssociatedOutputs = QtWidgets.QListView(self.tab_FX)
        self.listView_FXAssociatedOutputs.setGeometry(
            QtCore.QRect(20, 450, 321, 101))
        self.listView_FXAssociatedOutputs.setObjectName(
            "listView_FXAssociatedOutputs")
        self.comboBox_FXOutputs = QtWidgets.QComboBox(self.tab_FX)
        self.comboBox_FXOutputs.setGeometry(QtCore.QRect(350, 450, 231, 26))
        self.comboBox_FXOutputs.setObjectName("comboBox_FXOutputs")
        self.pushButton_AddAssociatedOutput = QtWidgets.QPushButton(
            self.tab_FX)
        self.pushButton_AddAssociatedOutput.setGeometry(
            QtCore.QRect(350, 490, 231, 41))
        self.pushButton_AddAssociatedOutput.setObjectName(
            "pushButton_AddAssociatedOutput")
        self.pushButton_addFX = QtWidgets.QPushButton(self.tab_FX)
        # self.pushButton_addFX.setEnabled(False)
        self.pushButton_addFX.setGeometry(QtCore.QRect(10, 570, 571, 41))
        self.pushButton_addFX.setObjectName("pushButton_addFX")
        self.lineEdit_FXName = QtWidgets.QLineEdit(self.tab_FX)
        self.lineEdit_FXName.setGeometry(QtCore.QRect(100, 221, 241, 21))
        self.lineEdit_FXName.setObjectName("lineEdit_FXName")
        self.label_29 = QtWidgets.QLabel(self.tab_FX)
        self.label_29.setGeometry(QtCore.QRect(10, 221, 43, 16))
        self.label_29.setObjectName("label_29")
        self.pushButton_deleteFX = QtWidgets.QPushButton(self.tab_FX)
        self.pushButton_deleteFX.setEnabled(False)
        self.pushButton_deleteFX.setGeometry(QtCore.QRect(350, 30, 231, 41))
        self.pushButton_deleteFX.setObjectName("pushButton_deleteFX")
        self.tab_MAIN.addTab(self.tab_FX, "")
        self.tab_Simulation = QtWidgets.QWidget()
        self.tab_Simulation.setObjectName("tab_Simulation")
        self.horizontalLayoutWidget_3 = QtWidgets.QWidget(self.tab_Simulation)
        self.horizontalLayoutWidget_3.setGeometry(
            QtCore.QRect(0, 450, 581, 121))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")
        self.horizontalLayout_Inputs = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget_3)
        self.horizontalLayout_Inputs.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_Inputs.setObjectName("horizontalLayout_Inputs")
        self.horizontalLayoutWidget_4 = QtWidgets.QWidget(self.tab_Simulation)
        self.horizontalLayoutWidget_4.setGeometry(
            QtCore.QRect(0, 10, 581, 431))
        self.horizontalLayoutWidget_4.setObjectName("horizontalLayoutWidget_4")
        self.verticalLayout_Outputs = QtWidgets.QVBoxLayout(
            self.horizontalLayoutWidget_4)
        self.verticalLayout_Outputs.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_Outputs.setObjectName("horizontalLayout_Outputs")
        self.tab_MAIN.addTab(self.tab_Simulation, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tab_MAIN.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Installation Simulator"))
        self.pushButton_selectInstallation.setText(
            _translate("MainWindow", "Select"))
        self.label_27.setText(_translate("MainWindow", "Name :"))
        self.label_28.setText(_translate("MainWindow", "New installation :"))
        self.pushButton_addInstallation.setText(
            _translate("MainWindow", "Add Installation"))
        self.pushButton_deleteInstallation.setText(
            _translate("MainWindow", "Delete"))
        self.label_30.setText(_translate("MainWindow", "Name :"))
        self.checkBox_Pin.setText(_translate(
            "MainWindow", "Microcontroller pin :"))
        self.label_18.setText(_translate("MainWindow", "New device :"))
        self.pushButton_newDeviceOutput.setText(
            _translate("MainWindow", "Add output"))
        self.pushButton_DeviceDelete.setText(
            _translate("MainWindow", "Delete"))
        self.pushButton_newDeviceInput.setText(
            _translate("MainWindow", "Add Input"))
        self.tab_MAIN.setTabText(self.tab_MAIN.indexOf(
            self.tab_instal), _translate("MainWindow", "Installation Setup"))
        self.label.setText(_translate("MainWindow", "Effects"))
        self.label_2.setText(_translate("MainWindow", "New Input:"))
        self.label_3.setText(_translate("MainWindow", "Effect type :"))
        self.pushButton_configureParameters.setText(
            _translate("MainWindow", "Configure parameters"))
        self.label_4.setText(_translate("MainWindow", "Associated Inputs:"))
        self.pushButton_addAssociatedInput.setText(
            _translate("MainWindow", "Add as associated input"))
        self.label_5.setText(_translate("MainWindow", "Associated Output:"))
        self.pushButton_AddAssociatedOutput.setText(
            _translate("MainWindow", "Add as associated output"))
        self.pushButton_addFX.setText(_translate("MainWindow", "Add effect"))
        self.label_29.setText(_translate("MainWindow", "Name :"))
        self.pushButton_deleteFX.setText(_translate("MainWindow", "Delete"))
        self.tab_MAIN.setTabText(self.tab_MAIN.indexOf(
            self.tab_FX), _translate("MainWindow", "Effect Assignation"))
        self.tab_MAIN.setTabText(self.tab_MAIN.indexOf(
            self.tab_Simulation), _translate("MainWindow", "Simulation"))

    def assignActions(self):
        self.pushButton_addInstallation.clicked.connect(self.addInstallation)
        self.pushButton_deleteInstallation.clicked.connect(
            self.deleteInstallation)
        self.pushButton_newDeviceOutput.clicked.connect(
            self.openNewOutputWindow)
        self.pushButton_selectInstallation.clicked.connect(
            self.selectInstallation)

    def addViewModels(self):
        self.ListModel_Installation = QStringListModel()
        self.ListView_installationList.setModel(
            self.ListModel_Installation)
        self.ListModel_Device = QStringListModel()
        self.ListView_deviceListDevices.setModel(
            self.ListModel_Device)
        self.ListModel_FX = QStringListModel()
        self.listView_FX.setModel(self.ListModel_FX)

    def updateModel(self, listModel, names):
        listModel.setStringList(names)

    def changeFocusTo(self, group, newFocus):
        group.setCurrentWidget(newFocus)

    def nameIsValid(self, name, dict):
        return name.strip() and name not in dict

    def addInstallation(self):
        name = self.getNewInstallationName()
        if self.nameIsValid(name, self.model.installations):
            installation = Installation(name)
            self.model.addInstallation(installation)
            self.selectedInstallation = installation
            self.updateModel(self.ListModel_Installation,
                             self.model.installationNames())
            self.installationsExist()

    def deleteInstallation(self):
        indexes = self.ListView_installationList.selectedIndexes()
        for index in indexes:
            name = str(self.ListModel_Installation.itemData(index)[0])
            self.model.removeInstallation(name)
        self.selectedInstallation = None
        self.installationsExist()

    def selectInstallation(self):
        indexes = self.ListView_installationList.selectedIndexes()
        for index in indexes:
            name = str(self.ListModel_Installation.itemData(index)[0])
            self.selectedInstallation = self.model.installation(name)
            self.installationsExist()

    def installationsExist(self):
        installationsExist = bool(self.model.installations)
        self.updateModel(self.ListModel_Installation,
                         self.model.installationNames())
        self.groupBox_Devices.setEnabled(installationsExist)
        self.pushButton_deleteInstallation.setEnabled(installationsExist)
        self.pushButton_selectInstallation.setEnabled(installationsExist)
        if not installationsExist:
            self.selectedInstallation = None
            self.clearDeviceView()
        self.inputsAndOutputsExist()

    def inputsAndOutputsExist(self):
        inputsExist = outputsExist = False
        if self.selectedInstallation:
            inputs = self.selectedInstallation.inputs()
            outputs = self.selectedInstallation.outputs()
            inputsExist = bool(inputs)
            outputsExist = bool(outputs)
        self.tab_FX.setEnabled(inputsExist and outputsExist)
        if self.selectedInstallation:
            names = self.selectedInstallation.deviceNames()
        else:
            names = []
        self.updateModel(self.ListModel_Device, names)

        if inputsExist and outputsExist:
            self.inputOutputComboBox(inputs, outputs)
            self.fillFXTypeComboBox()
        else:
            self.clearDeviceComboBoxes()
            self.clearFXView()
        self.associatedFXExist()

    def clearDeviceComboBoxes(self):
        self.comboBox_FXInputs.clear()
        self.comboBox_FXOutputs.clear()
        self.comboBox_FXType.clear()

    def clearDeviceView(self):
        self.updateModel(self.ListModel_Device, [])
        self.inputsAndOutputsExist()

    def clearFXView(self):
        self.updateModel(self.ListModel_FX, [])

    def inputOutputComboBox(self, inputs, outputs):
        self.clearDeviceComboBoxes()
        for key in inputs:
            self.comboBox_FXInputs.addItem(key)
        for key in outputs:
            self.comboBox_FXOutputs.addItem(key)

    def fillFXTypeComboBox(self):
        keys = LED_FX_DICTIONARY.keys()
        self.comboBox_FXType.addItems(keys)

    def associatedFXExist(self):
        associatedFXExist = False
        if self.selectedInstallation:
            associatedFXExist = bool(
                self.selectedInstallation.fxManager.assembledFXs)
            self.updateModel(self.ListModel_FX,
                             self.selectedInstallation.fxNames())
        self.tab_Simulation.setEnabled(associatedFXExist)
        self.pushButton_deleteFX.setEnabled(associatedFXExist)
        if associatedFXExist:
            self.enableSimulation()
        else:
            self.clearFXView()
            self.clearSimulation()

    def enableSimulation(self):
        self.clearSimulation()
        self.simulationSetup()

    def simulationSetup(self):
        self.simulatedOutputs = []
        self.addSimulatedInputs()
        self.addSimulatedOutputs()

    def addSimulatedInputs(self):
        self.simulatedInputs = []
        for device in self.selectedInstallation.inputs().values():
            simulatedinput = SimulatedBooleanInput(
                self.horizontalLayout_Inputs, device)
            self.simulatedInputs.append(simulatedinput)

    def addSimulatedOutputs(self):
        for device in self.selectedInstallation.outputs().values():
            simulatedinput = SimulatedLedStripOutput(
                self.verticalLayout_Outputs, device)
            self.simulatedOutputs.append(simulatedinput)

    def clearSimulation(self):
        for i in reversed(range(self.horizontalLayout_Inputs.count())):
            self.horizontalLayout_Inputs.itemAt(i).widget().setParent(None)
        for i in reversed(range(self.verticalLayout_Outputs.count())):
            self.verticalLayout_Outputs.itemAt(i).widget().setParent(None)

    def getNewInstallationName(self):
        return self.lineEdit_installationName.text()

    def getDeviceName(self):
        name = self.lineEdit_DeviceName.text()
        self.nameIsValid(
            name, self.selectedInstallation.devices)
        return name

    def getDevicePin(self):
        pin = None
        if self.checkBox_Pin.isChecked():
            pin = self.lineEdit_Pin.text()
        return pin

    def openNewOutputWindow(self):
        outputframe = Ui_NewOutputWindow(
            self, self.getDeviceName(), self.getDevicePin())
        outputframe.show()

    def resetDeviceForm(self):
        self.lineEdit_DeviceName.clear()
        self.lineEdit_DeviceName.hasFocus()

    def addDevice(self, device):
        self.model.addDevice(self.selectedInstallation, device)
        self.resetDeviceForm()
        self.inputsAndOutputsExist()


class Ui_NewOutputWindow(QMainWindow):
    def __init__(self, parent, name=None, pin=None):
        flags = QtCore.Qt.WindowFlags()
        flags |= QtCore.Qt.FramelessWindowHint
        super().__init__(parent, flags)
        self.parent = parent
        self.name = name
        self.pin = pin
        self.setupUi(self)
        self.assignActions()
        self.labelOutputName.setText(self.name)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(258, 130)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 241, 91))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.pushButton_outputAdd = QtWidgets.QPushButton(self.frame)
        self.pushButton_outputAdd.setGeometry(QtCore.QRect(0, 50, 151, 41))
        self.pushButton_outputAdd.setObjectName("pushButton_outputAdd")
        self.pushButton_outputCancel = QtWidgets.QPushButton(self.frame)
        self.pushButton_outputCancel.setGeometry(QtCore.QRect(150, 50, 91, 41))
        self.pushButton_outputCancel.setObjectName("pushButton_outputCancel")
        self.labelOutputName = QtWidgets.QLabel(self.frame)
        self.labelOutputName.setGeometry(QtCore.QRect(130, 0, 101, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.labelOutputName.setFont(font)
        self.labelOutputName.setObjectName("label_outputName")
        self.layoutWidget = QtWidgets.QWidget(self.frame)
        self.layoutWidget.setGeometry(QtCore.QRect(11, 20, 224, 23))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout_17 = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_17.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_17.setObjectName("horizontalLayout_17")
        self.label_20 = QtWidgets.QLabel(self.layoutWidget)
        self.label_20.setObjectName("label_20")
        self.horizontalLayout_17.addWidget(self.label_20)
        self.lineEdit_LedStripLedCount = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit_LedStripLedCount.setObjectName(
            "lineEdit_LedStripLedCount")
        self.horizontalLayout_17.addWidget(self.lineEdit_LedStripLedCount)
        self.label_8 = QtWidgets.QLabel(self.frame)
        self.label_8.setGeometry(QtCore.QRect(0, 0, 131, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_outputAdd.setText(_translate("MainWindow", "Add"))
        self.pushButton_outputCancel.setText(
            _translate("MainWindow", "Cancel"))
        self.labelOutputName.setText(_translate("MainWindow", "OutputName"))
        self.label_20.setText(_translate("MainWindow", "LED count :"))
        self.label_8.setText(_translate("MainWindow", "Output -LED strip :"))

    def assignActions(self):
        self.pushButton_outputAdd.clicked.connect(self.addOutput)

    def addOutput(self):
        isValid, parameters = self.ledStripParametersareValid()
        if isValid:
            name, ledCount, pin = parameters
            ledStrip = LedStripOutput(name, ledCount, pin)
            self.parent.addDevice(ledStrip)
            self.close()
        else:
            self.resetFields()

    def resetFields(self):
        self.lineEdit_LedStripLedCount.clear()

    def getLedCount(self):
        return self.lineEdit_LedStripLedCount.text()

    def ledStripParametersareValid(self):
        name = self.name
        ledCount = self.getLedCount()
        pin = self.pin
        warnings = ""
        if name:
            if name in self.parent.selectedInstallation.devices:
                warnings += ("- Name alerady exist -")
        else:
            warnings += ("- Must indicate name -")

        if ledCount:
            try:
                ledCount = int(ledCount)
            except:
                warnings += "- Led Count must be a number -"
        else:
            warnings += "- Must indicate LedCount -"

        if pin:
            if pin.strip() == "":
                warnings += "- Must indicate pin -"

        if warnings:
            WarningBox(warnings)
            return (False, None)
        else:
            return (True, (name, ledCount, pin))


class WarningBox(QWidget):
    def __init__(self, warningText):
        super().__init__()
        self.title = 'Warning'
        self.setGeometry(QtCore.QRect(0, 0, 320, 200))
        self.left = 200
        self.top = 200
        self.width = 320
        self.height = 200
        self.initUI(warningText)

    def initUI(self, warningText):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        buttonReply = QMessageBox.warning(
            self, 'Warning', warningText, QMessageBox.Ok)
        if buttonReply == QMessageBox.Ok:
            self.close()
        self.show()

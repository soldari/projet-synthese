from device import *
from fxManager import *


class Installation:
    def __init__(self, name):
        self.name = name
        self.devices = {}
        self.fxManager = FXManager(self)

    def addDevice(self, device: Device):
        self.devices[device.name] = device

    def removeDevice(self, devicename):
        device = self.devices[devicename]
        del device

    def deviceNames(self):
        return self.devices.keys()

    def deviceSubtypesNames(self, subtype: DeviceSubType):
        deviceNames = []
        for key, value in self.devices.items():
            if value.deviceSubType == subtype:
                deviceNames.append(key)
        return deviceNames

    def fxNames(self):
        fxNames = []
        for fx in self.fxManager.assembledFXs:
            fxNames.append(fx.name)
        return fxNames

    def inputs(self):
        devices = {}
        for key, value in self.devices.items():
            if value.deviceType == DeviceType.INPUT:
                devices[key] = value
        return devices

    def outputs(self):
        devices = {}
        for key, value in self.devices.items():
            if value.deviceType == DeviceType.OUTPUT:
                devices[key] = value
        return devices

    def run(self):
        self.fxManager.runAll()


class DefaultInstallation(Installation):
    def __init__(self):
        super(DefaultInstallation, self).__init__("Default installation")
        self.setUp()
        self.fxManager = DefaultFXManager(self)

    def setUp(self):
        for i in range(5):
            name = "ledStrip " + str(i+1)
            device = LedStripOutput(name, 88)
            self.addDevice(device)
        for i in range(2):
            name = "motionDetector " + str(i+1)
            device = BooleanInput(name)
            self.addDevice(device)


class PresentationInstallation(Installation):
    def __init__(self):
        super(PresentationInstallation, self).__init__(
            "Presentation Installation")
        self.setUp()
        self.fxManager = PresentationFXManager(self)

    def setUp(self):
        for i in range(6):
            name = "ledStrip " + str(i+1)
            device = LedStripOutput(name, 88)
            self.addDevice(device)

        name = "Duration"
        device = BooleanInput(name)
        self.addDevice(device)

        name = "Speed"
        device = BooleanInput(name)
        self.addDevice(device)

        name = "Density"
        device = BooleanInput(name)
        self.addDevice(device)

        name = "Color"
        device = BooleanInput(name)
        self.addDevice(device)

        name = "Perlin Rainbow"
        device = BooleanInput(name)
        self.addDevice(device)

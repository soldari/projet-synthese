
import itertools
import logging

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPainter, QColor
from PyQt5.QtWidgets import QWidget, QGraphicsItem, QGraphicsRectItem, QGraphicsScene, QGraphicsView, QStyleOptionGraphicsItem
from PyQt5.QtCore import QRect, QRectF, pyqtSlot

from constant import *
from overrides import *


# # #   I N P U T
class SimulatedBooleanInput (QWidget):
    def __init__(self, parent, input):
        super(SimulatedBooleanInput, self).__init__()
        self.parent = parent
        self.device = input
        self.setUp()
        self.translateUi()
        self.assignActions()

    def assignActions(self):
        self.pushButton.clicked.connect(self.device.notifyObservers)

    def delete(self):
        self.parent.removeWidget(self)

    def setUp(self):
        self.layout = QtWidgets.QVBoxLayout(self)
        spacerItemTop = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout.addItem(spacerItemTop)
        self.hLayout = QtWidgets.QHBoxLayout()
        spacerItemL = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.hLayout.addItem(spacerItemL)
        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setMinimumSize(QtCore.QSize(100, 50))
        self.hLayout.addWidget(self.pushButton)
        spacerItemR = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.hLayout.addItem(spacerItemR)
        self.layout.addLayout(self.hLayout)
        self.label = QtWidgets.QLabel(self)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.layout.addWidget(self.label)
        self.parent.addWidget(self)

    def translateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.pushButton.setText(_translate("Frame", "Trigger"))
        self.label.setText(_translate("Frame", self.device.name))

# # #   O U T P U T
class SimulatedLedStripOutput(QWidget):
    def __init__(self, parent, output):
        super(SimulatedLedStripOutput, self).__init__()
        self.parent = parent
        self.output = output
        self.setupUi()
        self.translateUi()

    @pyqtSlot()
    def updateView(self):
        for (simLed, led) in zip(self.scene.items(), self.output.leds):
            r = led["r"]
            g = led["g"]
            b = led["b"]
            a = led["brightness"]
            color = QColor(r, g, b, a)
            simLed.setBrush(color)

    def setupUi(self):
        self.layoutwidget = QtWidgets.QWidget(self)
        self.layout = QtWidgets.QHBoxLayout(self.layoutwidget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel(self.layoutwidget)
        self.label.setMaximumSize(QtCore.QSize(100, 20))
        self.layout.addWidget(self.label)

        self.scene = QGraphicsScene()
        self.view = QGraphicsView(self.scene)
        self.setUpScene()
        self.layout.addWidget(self.view)
        self.parent.addWidget(self)

    def translateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Form", self.output.name))

    def setUpScene(self):
        for i in range(self.output.ledCount):
            rect  = QGraphicsRectItem((LED_PIXEL_SIZE + LED_SPACING) * i, 10, LED_PIXEL_SIZE, LED_PIXEL_SIZE)
            self.scene.addItem(rect)
    
    def delete(self):
        self.parent.removeWidget(self)

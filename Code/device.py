import logging
import enum  # for device types
import numpy as np
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal

from abc import ABCMeta  # for device abstract class


####  DEVICE TYPES  ####
class DeviceType(enum.Enum):
    INPUT = 0
    OUTPUT = 1


class DeviceSubType(enum.Enum):
    BOOLEAN = 0
    VALUE = 1
    INCREMENTAL = 2
    TIME = 3
    LEDSTRIP = 10


class Device(QObject):
    def __init__(self, name, pin):
        super().__init__()
        self.name = name
        self.pin = pin
        self.deviceType = None
        self.deviceSubType = None


####  INPUTS  ####
class Input(Device):
    def __init__(self, name, pin=None):
        super(Input, self).__init__(name, pin)
        self.deviceType = DeviceType.INPUT
        self.inputType = None
        self.observers = []

    def registerObserver(self, fx):
        logging.debug(self.name + " registered to " + fx.name)
        self.observers.append(fx)

    @pyqtSlot()
    def notifyObservers(self):
        for observer in self.observers:
            observer.start()


class BooleanInput(Input):
    def __init__(self, name,  pin=None):
        super(BooleanInput, self).__init__(name, pin)
        self.deviceSubType = DeviceSubType.BOOLEAN


####  OUTPUTS  ####
class Output(Device):
    def __init__(self, name, pin=None):
        super(Output, self).__init__(name, pin)
        self.deviceType = DeviceType.OUTPUT


class LedStripOutput(Output):
    def __init__(self, name, ledCount: int, pin=None):
        super(LedStripOutput, self).__init__(name, pin)
        self.deviceSubType = DeviceSubType.LEDSTRIP
        self.ledCount = ledCount
        self.leds = np.zeros(ledCount, dtype='int, int, int, int')
        self.leds.dtype.names = ("r", "g", "b", "brightness")

    def setLedColor(self, ledPosition: int, r, g, b):
        self.leds[ledPosition]["r"] = r
        self.leds[ledPosition]["g"] = g
        self.leds[ledPosition]["b"] = b

    def setLedBrightness(self, ledPosition: int, brightness):
        self.leds[ledPosition]["brightness"] = brightness

    def show(self): pass

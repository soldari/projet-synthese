import logging
from PyQt5.QtCore import QThread, QTimer

from fx import *
from constant import *

LED_FX_DICTIONARY = {
    "Twinkle": TwinkleLedFX,
    "PerlinColor": PerlinColorLedFX
}


class AssembledFX:
    def __init__(self, name, fx: FX, inputs, outputs):
        self.name = name
        self.fx = fx
        self.outputs = outputs
        self.inputs = inputs
        self.setUp()

    def setUp(self):
        self.fx.setOutputs(self.outputs)
        self.fx.setUp()
        self.subscribe(self.inputs)

    def subscribe(self, inputs):
        for input in inputs:
            input.registerObserver(self)

    def start(self):
        self.fx.start()

    def outputNames(self):
        """
        Returns output names for the console
        """
        outputNames = []
        for output in self.outputs:
            outputNames.append(output.name)
        return outputNames


class FXManager:
    """
    Class whose purpose it is to contain, associate, and manage 
    FXs with installation inputs and outputs.

    @param (objet) installation : Installation
    @instance_variable (dict) FXs : containing FX subclass
    @instance_variable (list) assembledFXs :  containing AssembledFx objects
    @instance_variable (object) installation : Installation object
    """

    def __init__(self, installation):
        self.running = False
        self.assembledFXs = []
        self.installation = installation

    def addAssembledFx(self, assembledFX):
        """
        Adds a new AssembledFX to the FXManager.

        @param (object) fx: FX
        """
        self.assembledFXs.append(assembledFX)

    def removeAssembledEffect(self, name):
        """
        Removes a AssembledFX to the FXManager.

        @param (string) name: AssembledEffect
        """
        assembledFX = [AFX for AFX in self.assembledFXs if AFX.name == name]
        self.assembledFXs.remove(assembledFX)

    def runAll(self):
        """
        Calls the tick method of all AssembledFXs
        """
        for assembledFX in self.assembledFXs:
            assembledFX.fx.run()


class DefaultFXManager(FXManager):
    """
    FXManager subclass whose purpose it preconfigure AssociatedFXs 
    on the Default Installation.
    """

    def __init__(self, installation):
        super(DefaultFXManager, self).__init__(installation)
        self.setUp()

    def setUp(self):
        self.addAssembledFx(self.defaultAssembledFX1())
        self.addAssembledFx(self.defaultAssembledFX2())
        self.addAssembledFx(self.defaultAssembledFX3())
        self.addAssembledFx(self.defaultAssembledFX4())

    def defaultAssembledFX1(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['motionDetector 1'])

        colors = [(255, 255, 255)]
        duration = 2000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "FX1"

        return AssembledFX(name, fx, inputs, outputs)

    def defaultAssembledFX2(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 4'])
        outputs.append(self.installation.devices['ledStrip 5'])

        inputs = []
        inputs.append(self.installation.devices['motionDetector 1'])

        colors = [(255, 0, 0)]
        duration = 5000
        density = 0.5
        speed = .5
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = LowTwinkleLedFX(parameters=parameters)

        name = "FX2"

        return AssembledFX(name, fx, inputs, outputs)

    def defaultAssembledFX3(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 2'])
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])

        inputs = []
        inputs.append(self.installation.devices['motionDetector 2'])

        colors = [(0, 255, 0), (200, 255, 50), (50, 255, 200)]
        duration = 2000
        density = 0.3
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)
        fx = HighTwinkleLedFX(parameters=parameters)

        name = "FX3"

        return AssembledFX(name, fx, inputs, outputs)

    def defaultAssembledFX4(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['motionDetector 2'])

        duration = 10000

        parameters = LedFXParameters(duration=duration)
        fx = PerlinColorLedFX(parameters=parameters)

        name = "FX4"

        return AssembledFX(name, fx, inputs, outputs)


class PresentationFXManager(FXManager):
    """
    FXManager subclass whose purpose it preconfigure AssociatedFXs 
    on the Presentation Installation.
    """

    def __init__(self, installation):
        super(PresentationFXManager, self).__init__(installation)
        self.setUp()

    def setUp(self):
        self.addAssembledFx(self.presentationAssembledFXDuration1())
        self.addAssembledFx(self.presentationAssembledFXDuration2())
        self.addAssembledFx(self.presentationAssembledFXDuration3())
        self.addAssembledFx(self.presentationAssembledFXDensity1())
        self.addAssembledFx(self.presentationAssembledFXDensity2())
        self.addAssembledFx(self.presentationAssembledFXDensity3())
        self.addAssembledFx(self.presentationAssembledFXspeed1())
        self.addAssembledFx(self.presentationAssembledFXspeed2())
        self.addAssembledFx(self.presentationAssembledFXspeed3())
        self.addAssembledFx(self.presentationAssembledFXColor1())
        self.addAssembledFx(self.presentationAssembledFXColor2())
        self.addAssembledFx(self.presentationAssembledFXColor3())
        self.addAssembledFx(self.presentationAssembledFXPerlinRainbow())

    def presentationAssembledFXDuration1(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['Duration'])

        colors = [(255, 255, 255)]
        duration = 2000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Short Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXDuration2(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])

        inputs = []
        inputs.append(self.installation.devices['Duration'])

        colors = [(255, 255, 255)]
        duration = 3000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Medium Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXDuration3(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 5'])
        outputs.append(self.installation.devices['ledStrip 6'])

        inputs = []
        inputs.append(self.installation.devices['Duration'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Long Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXDensity1(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['Density'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.05
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Sparse Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXDensity2(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])

        inputs = []
        inputs.append(self.installation.devices['Density'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Medium Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXDensity3(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 5'])
        outputs.append(self.installation.devices['ledStrip 6'])

        inputs = []
        inputs.append(self.installation.devices['Density'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.5
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Long Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXspeed1(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['Speed'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.2
        speed = 0.2
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Slow Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXspeed2(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])

        inputs = []
        inputs.append(self.installation.devices['Speed'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.2
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Normal Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXspeed3(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 5'])
        outputs.append(self.installation.devices['ledStrip 6'])

        inputs = []
        inputs.append(self.installation.devices['Speed'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.2
        speed = 5
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Fast Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXColor1(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['Color'])

        colors = [(255, 255, 255)]
        duration = 4000
        density = 0.5
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "White Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXColor2(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])

        inputs = []
        inputs.append(self.installation.devices['Color'])

        colors = [(255, 0, 0), (255, 100, 0), (255, 50, 0),
                  (255, 0, 100), (255, 50, 0), (255, 100, 100),
                  (255, 50, 50), (255, 50, 100), (255, 100, 50)]
        duration = 5000
        density = 0.5
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Red Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXColor3(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 5'])
        outputs.append(self.installation.devices['ledStrip 6'])

        inputs = []
        inputs.append(self.installation.devices['Color'])

        colors = [(0, 255, 0), (200, 255, 50), (50, 255, 200)]
        duration = 5000
        density = 0.5
        speed = 1
        parameters = LedFXParameters(
            colors=colors, duration=duration, density=density, speed=speed)

        fx = TwinkleLedFX(parameters=parameters)

        name = "Green Twinkle"

        return AssembledFX(name, fx, inputs, outputs)

    def presentationAssembledFXPerlinRainbow(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])
        outputs.append(self.installation.devices['ledStrip 3'])
        outputs.append(self.installation.devices['ledStrip 4'])
        outputs.append(self.installation.devices['ledStrip 5'])
        outputs.append(self.installation.devices['ledStrip 6'])

        inputs = []
        inputs.append(self.installation.devices['Perlin Rainbow'])
        duration = 6000

        parameters = LedFXParameters(duration=duration)
        fx = PerlinColorLedFX(parameters=parameters)

        name = "Perlin Rainbow"

        return AssembledFX(name, fx, inputs, outputs)

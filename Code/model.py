from installation import *
from constant import *


class Model(QObject):
    def __init__(self):
        super(Model, self).__init__(None)
        self.installations = {}
        self.setUp()
    

    def setUp(self):
        defautInstallation = DefaultInstallation()
        self.addInstallation(defautInstallation)
        presentationInstallation = PresentationInstallation()
        self.addInstallation(presentationInstallation)
    
    def installation (self, installationName):
        return self.installations[installationName]

    def addInstallation(self, installation: Installation):
        self.installations[installation.name] = installation

    def removeInstallation(self, installationName):
        del self.installations[installationName]

    def installationNames(self):
        return self.installations.keys()

    def addDevice(self, installation: Installation, device: Device):
        installation.addDevice(device)

    def removeDevice(self, deviceName, installation):
        installation.removeDevice(deviceName)

    def deviceNames(self, installation):
        return installation.deviceNames()

    def deviceSubtypeNames(self, installation: Installation, subtype: DeviceSubType):
        return installation.deviceSubtypesNames(subtype)

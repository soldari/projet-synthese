import sys
import logging
import numpy as np
# from PyQt5.QtCore import *
import qtmodern.styles
import qtmodern.windows
from console import ConsoleWindow
from model import Model

from PyQt5.QtWidgets import QApplication


def window(model):
    app = QApplication(sys.argv)
    win = ConsoleWindow(model)
    qtmodern.styles.dark(app)
    # dependency qtmodern
    mw = qtmodern.windows.ModernWindow(win)
    mw.show()
    thread = app.thread()
    win.moveToThread(thread)
    sys.exit(app.exec_())


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    np.random.seed(0)
    m = Model()
    window(m)


import logging
from constant import *


class State:
    pass


class FXStateMachine:
    def __init__(self):
        self.currentState = InactiveFXState(self)
        self.timeElapsed = 0

    def setCurrentState(self, newState):
        self.currentState = newState(self)

    def run(self):
        self.currentState.run()

    def start(self):
        self.currentState.start()

    def stop(self):
        self.currentState.stop()
    
    def setUp(self): pass

    def tick(self): pass


class FXState(State):
    def __init__(self, fx: FXStateMachine):
        self.fx = fx

    def start(self): pass

    def stop(self): pass

    def run(self): pass


class ActiveFXState(FXState):
    def __init__(self, fx):
        super(ActiveFXState, self).__init__(fx)

    def start(self):
        logging.debug("already started : state active")
        pass

    def stop(self):
        self.fx.displayStop()
        self.fx.setCurrentState(InactiveFXState)
        logging.debug("stopping : state switching to inactive")

    def run(self):
        # logging.debug("running and ticking : state active")
        self.fx.timeElapsed += TIMER_INTERVAL
        self.fx.tick()

    
class InactiveFXState(FXState):
    def __init__(self, fx):
        super(InactiveFXState, self).__init__(fx)

    def start(self):
        logging.debug("starting : state switching to active")
        self.fx.timeElapsed = 0
        self.fx.displayStart()
        self.fx.setCurrentState(ActiveFXState)


    def stop(self):
        logging.debug("already stopped : state inactive")
        pass

    def run(self):
        #logging.debug("cannot run : state inactive")
        pass

import logging
import unittest

from model import *
from installation import *
from device import *
from stateMachine import *
from fx import *
from fxManager import *


class TestModel(unittest.TestCase):
    def setUp(self):
        self.model = Model()
        installation = Installation("test Installation 1")
        self.model.addInstallation(installation)

    def testModelAddInstallation(self):
        installation = Installation("test Installation 2")
        self.model.addInstallation(installation)
        assert "test Installation 2" in self.model.installations, "Installation not added"
        assert isinstance(
            self.model.installations["test Installation 2"], Installation)

    def testModelRemoveInstallation(self):
        self.model.removeInstallation("test Installation 1")
        assert "test Installation 1" not in self.model.installations, "Installation not removed"


class TestInstallation(unittest.TestCase):
    def setUp(self):
        self.installation = Installation("Installation")

    def testInstallationAddOutput(self):
        ledStrip = LedStripOutput("LedStrip", 150, "D18")
        self.installation.addDevice(ledStrip)
        assert isinstance(
            self.installation.devices["LedStrip"], Output), "LedStrip not instance of Output"

    def testInstallationAddInput(self):
        booleaninput = BooleanInput("booleanInput", pin="D6")
        self.installation.addDevice(booleaninput)
        assert isinstance(
            self.installation.devices["booleanInput"], Input), "booleanInput not instance of Input"


class TestLedStrip(unittest.TestCase):
    def setUp(self):
        self.len = 5
        self.ledStrip = LedStripOutput("ledStrip", self.len)

    def testLedStripLength(self):
        assert len(
            self.ledStrip.leds) == self.len, "ledString length not appropriate"

    def testLedStripColor(self):
        r = g = b = 25
        self.ledStrip.setLedColor(0, r, g, b)
        assert self.ledStrip.leds[0]["r"] == r, "Led color not appropriate"

    def testLedStripBrightness(self):
        brightness = 10
        self.ledStrip.setLedBrightness(0, brightness)
        assert self.ledStrip.leds[0]["brightness"] == brightness, "Led brightness not appropriate"


class TestDefaultInstallationRun(unittest.TestCase):
    def setUp(self):
        self.defaultInstallation = DefaultInstallation()

    def testDefaultInstallationsetup(self):
        logging.debug("DEFAULT INSTALLATION: testing registered devices")
        logging.debug(self.defaultInstallation.deviceNames())
        assert 'ledStrip 5' in self.defaultInstallation.devices, "Output devices not properly added in Default Installation"
        assert 'motionDetector 2' in self.defaultInstallation.devices, "Input devices not properly added in Default Installation"


class TestStateMachine(unittest.TestCase):
    def setUp(self):
        self.stateMachine = FXStateMachine()

    def testStates(self):
        logging.debug("STATE MACHINE : testing state behavior")
        self.stateMachine.run()
        self.stateMachine.stop()
        self.stateMachine.start()
        assert isinstance(self.stateMachine.currentState, ActiveFXState)
        self.stateMachine.run()
        self.stateMachine.start()
        self.stateMachine.stop()
        assert isinstance(self.stateMachine.currentState, InactiveFXState)
        self.stateMachine.run()


class TestLedFX(unittest.TestCase):
    def setUp(self):
        self.ledStrips = []
        ledStrip1 = LedStripOutput("test strip1", 200)
        ledStrip2 = LedStripOutput("test strip2", 200)
        self.ledStrips.append(ledStrip1)
        self.ledStrips.append(ledStrip2)

    def testLedFx(self):
        self.ledFx = LedFX(ledStrips=self.ledStrips)

    def testTwinkleLedFx(self):
        colors = [(239, 0, 35), (85, 103, 22), (244, 0, 30)]
        duration = 5000
        density = 0.5
        parameters = LedFXParameters(colors=colors, duration=duration)
        self.twinkleFx = TwinkleLedFX(self.ledStrips, parameters)


class TestFXManager(unittest.TestCase):
    def setUp(self):
        self.installation = DefaultInstallation()
        self.fxManager = FXManager(self.installation)

    def assembledEffectTest(self):
        outputs = []
        outputs.append(self.installation.devices['ledStrip 1'])
        outputs.append(self.installation.devices['ledStrip 2'])

        inputs = []
        inputs.append(self.installation.devices['motionDetector 1'])

        colors = [(255, 255, 255)]
        duration = 20
        density = 0.5
        parameters = LedFXParameters(colors=colors, duration=duration)

        fx = TwinkleLedFX(parameters=parameters)

        name = "test"

        return AssembledFX(name, fx, inputs, outputs)

    def testAssembledFX(self):
        assembledFX = self.assembledEffectTest()

    def testRunAssmbledFX(self):
        logging.debug("FX MANAGER : testing runAll")
        self.fxManager.addAssembledFx(self.assembledEffectTest())
        self.fxManager.runAll()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()

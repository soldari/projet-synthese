from abc import ABCMeta, ABC  # for fx abstract class
import logging
import random
import math
import colorsys

from overrides import *
from device import *
from stateMachine import *
from perlinNoise import *


class FX(FXStateMachine):
    def __init__(self, outputs, parameters):
        self.setCurrentState(InactiveFXState)
        self.outputs = outputs
        self.parameters = parameters

    def setOutputs(self, outputs):
        self.outputs = outputs


class LedFXParameters:
    def __init__(self, colors=None, duration=None, density=None, speed=1):
        self.colors = colors
        self.duration = duration
        self.density = density
        self.speed = speed


class LedFX(FX):
    def __init__(self, ledStrips=None, parameters=None):
        super(LedFX, self).__init__(outputs=ledStrips, parameters=parameters)
        self.timeElapsed = 0

    def tick(self):
        pass

    def displayStart(self):
        pass

    def displayStop(self):
        pass

    def getLedcount(self):
        totalLedCount = 0
        for ledStrip in self.outputs:
            totalLedCount += ledStrip.ledCount
        return totalLedCount

    def longestOutput(self):
        longestCount = 0
        for ledStrip in self.outputs:
            if ledStrip.ledCount > longestCount:
                longestCount = ledStrip.ledCount
        return longestCount

    def highLuminosity(self):
        for ledStrip in self.outputs:
            for led in ledStrip.leds:
                led["brightness"] = 255

    def lowLuminosity(self):
        for ledStrip in self.outputs:
            for led in ledStrip.leds:
                led["brightness"] = 0

    def reset(self):
        for ledStrip in self.outputs:
            for led in ledStrip.leds:
                led = [0][0][0][0]

    def hsv2rgb(self, h, s, v):
        return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h, s, v))

    def setParameters(self, parameters: LedFXParameters):
        self.parameters = parameters


class TwinkleLedFX(LedFX):
    def __init__(self, ledStrips=None, parameters=None):
        super(TwinkleLedFX, self).__init__(
            ledStrips=ledStrips, parameters=parameters)
        self.activeLeds = []
        self.sprites = []

    def setUp(self):
        self.spriteNb = int(self.parameters.density*self.getLedcount())
        self.newSpriteRange = math.ceil(
            self.spriteNb/20 * self.parameters.speed)
        logging.debug("Sprite nb = " + str(self.spriteNb))
        self.colors = self.parameters.colors or [(255, 255, 255)]

    def existingTwinkle(self, led):
        return led["brightness"] > 0

    def newRandomTwinkle(self):
        ledStrip = random.choice(self.outputs)
        led = random.choice(ledStrip.leds)
        if not self.existingTwinkle(led):
            color = random.choice(self.colors)
            self.sprites.append(Twinkle(led, color, self.parameters.speed))
            logging.debug("New twinkle")

    def tick(self):
        logging.debug("Duration " + str(self.parameters.duration) +
                      ", time elapsed " + str(self.timeElapsed))
        if self.timeElapsed < self.parameters.duration:
            if len(self.sprites) < self.spriteNb:
                for i in range(self.newSpriteRange):
                    self.newRandomTwinkle()
        for sprite in self.sprites:
            alive = sprite.tick()
            if not alive:
                self.sprites.remove(sprite)
        logging.debug("TwinkleEffect ticking")
        if not self.sprites:
            logging.debug("TwinkleEffect stoping")
            self.setCurrentState(InactiveFXState)


class Twinkle:
    def __init__(self, led, color, speed):
        self.led = led
        self.brightness = 5
        self.direction = 1
        self.led["r"], self.led["g"], self.led["b"] = color
        self.speed = speed
        self.led["brightness"] = self.brightness

    def tick(self):
        self.brightness += 25.5 * self.speed * self.direction
        if self.brightness >= 255:
            self.brightness = 255
            self.direction *= -1
        if self.brightness < 0:
            self.brightness = 0
        self.led["brightness"] = int(self.brightness)
        logging.debug("Twinkling at " + str(self.brightness))
        return self.brightness > 0

        return self.brightness <= 0


class LowTwinkleLedFX(TwinkleLedFX):
    @overrides(TwinkleLedFX)
    def newRandomTwinkle(self):
        ledStrip = random.choice(self.outputs)
        ledPosition = int(random.random() ** 2 * ledStrip.ledCount)
        led = ledStrip.leds[ledPosition]
        if not self.existingTwinkle(led):
            color = random.choice(self.colors)
            self.sprites.append(Twinkle(led, color, self.parameters.speed))
            logging.debug("New Low Twinkle at " + str(ledPosition))


class HighTwinkleLedFX(TwinkleLedFX):
    @overrides(TwinkleLedFX)
    def newRandomTwinkle(self):
        ledStrip = random.choice(self.outputs)
        ledPosition = int(math.sqrt(random.random())*ledStrip.ledCount)
        led = ledStrip.leds[ledPosition]
        if not self.existingTwinkle(led):
            color = random.choice(self.colors)
            self.sprites.append(Twinkle(led, color, self.parameters.speed))
            logging.debug("New High Twinkle at " + str(ledPosition))


class PerlinColorLedFX(LedFX):
    def __init__(self, ledStrips=None, parameters=None):
        super(PerlinColorLedFX, self).__init__(
            ledStrips=ledStrips, parameters=parameters)

    def setUp(self):
        self.length = self.longestOutput()
        self.width = len(self.outputs)
        self.height = self.parameters.duration

        self.position = 0
        self.noise = generate_fractal_noise_3d(
            (self.parameters.duration, self.width, self.length),
            (5*self.parameters.speed, 1, 4), tileable=(True, False, True))

    def noiseSnapshot(self, time):
        return self.noise[time]

    @overrides(LedFX)
    def displayStart(self):
        self.highLuminosity()

    @overrides(LedFX)
    def displayStop(self):
        self.lowLuminosity()
        self.setCurrentState(InactiveFXState)

    @overrides(LedFX)
    def tick(self):
        if self.timeElapsed < self.parameters.duration:
            if self.timeElapsed % 10 == 0:
                logging.debug("TIME ELAPSED " + str(self.timeElapsed))
            snapshot = self.noiseSnapshot(self.timeElapsed)
            for ledStrip, row in zip(self.outputs, snapshot):
                for led, dot in zip(ledStrip.leds, row):
                    density = (dot + 1)
                    r, g, b = self.hsv2rgb(density, 1, 1)
                    led["r"] = r
                    led["g"] = g
                    led["b"] = b
        else:
            self.displayStop()


class FlameLedFX(LedFX):
    def __init__(self, ledStrips=None, parameters=None):
        super(PerlinColorLedFX, self).__init__(
            ledStrips=ledStrips, parameters=parameters)
        self.backgroudColorMatrix = backgroudColor()
        self.fireColor = 0
        self.started = True

    @overrides(LedFX)
    def displayStart(self):
        self.fireColor += 1
        if self.fireColor == 255:
            self.started = True

    @overrides(LedFX)
    def displayStop(self):
        self.reset()

    @overrides(LedFX)
    def tick(self):
        if not self.started:
            self.displayStart()
